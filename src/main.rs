use minifb::{Scale, Key, Window, WindowOptions};
use std::fs;
use std::io::Read;
use std::num::Wrapping;

const WIDTH: usize = 64;
const HEIGHT: usize = 32;


fn main() {
    let mut chip8 = Chip8::new();

    let mut buffer = Vec::new();
    let mut f = fs::File::open("/Users/kittiphat/Downloads/Tetris [Fran Dachille, 1991].ch8").unwrap();
    f.read_to_end(&mut buffer).unwrap();

    println!("file is {} bytes", buffer.len());

    for (i, opcode) in buffer.iter().enumerate() {
        chip8.mem[512 + i] = *opcode
    }

    chip8.start();
}

struct Chip8 {
    // opcode
    // let opcode u8;

    // memory
    // 4096
    // let ...

    // 15 8-bit registers
    // V0, V1, ..., VE
    // let ...

    // index register I
    // let ...

    // program counter pc
    // let ...
    opcode: u16,
    mem: [u8; 4096],
    V: [u8; 16],
    I: usize,
    pc: usize,
    gfx: [u8; 64 * 32],
    stack: [usize; 16],
    sp: usize,
    key: [u8; 16],
    dt: u8, // delay timer
    st: u8, // sound timer

    window: Window
}

impl Chip8 {
    fn new() -> Chip8 {
        let font_set = [
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        ];

        let mut initial_mem = [0; 4096];

        for i in 0..80 {
            initial_mem[i] = font_set[i];
        }

        let mut window = Window::new(
            "Test - ESC to exit",
            WIDTH,
            HEIGHT,
            WindowOptions {
                scale: Scale::X16,
                ..WindowOptions::default()
            }
        )
            .unwrap_or_else(|e| {
                panic!("{}", e);
            });

        window.limit_update_rate(Some(std::time::Duration::from_millis(500 / 60)));

        Chip8 {
            pc: 0x200,
            opcode: 0,
            I: 0,
            sp: 0,
            mem: initial_mem,
            V: [0; 16],
            gfx: [0; 64 * 32],
            stack: [0; 16],
            key: [0; 16],
            dt: 0,
            st: 0,

            window: window
        }
    }

    fn start(&mut self) {
        let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

        while self.window.is_open() {
            self.run_cycle();

            if self.dt > 0 {
                self.dt -= 1
            }

            for i in 0..(WIDTH * HEIGHT) {
                if self.gfx[i] == 1 {
                    buffer[i] = 255 << 8; // write something more funny here!
                } else {
                    buffer[i] = 255 << 4; // write something more funny here!
                }
            }

            self.window
                .update_with_buffer(&buffer, WIDTH, HEIGHT)
                .unwrap();
        }

    }

    fn run_cycle(&mut self) {
        // fetch opcode
        self.opcode = (self.mem[self.pc] as u16) << 8 | self.mem[self.pc + 1] as u16;

        match self.opcode & 0xF000 {
            0x0000 => {
                match self.opcode {
                    0x0E0 => {
                        for i in self.gfx.iter_mut() {
                            *i = 0
                        }
                    },
                    0x0EE => {
                        self.pc = self.stack[self.sp];
                        self.sp -= 1;
                    },
                    _ => {}
                }
            },
            0x1000 => {
                let nnn = (self.opcode & 0x0FFF) as usize;
                self.pc = nnn - 2;
            },
            0x2000 => {
                let nnn = (self.opcode & 0x0FFF) as usize;
                self.sp += 1;
                self.stack[self.sp] = self.pc;
                self.pc = nnn - 2;
            },
            0x3000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let nn = (self.opcode & 0x00FF) as u8;
                if self.V[x] == nn {
                    self.pc += 2;
                }
            },
            0x4000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let nn = (self.opcode & 0x00FF) as u8;
                if self.V[x] != nn {
                    self.pc += 2;
                }
            },
            0x5000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let y = ((self.opcode & 0x00F0) >> 4) as usize;
                if self.V[x] == self.V[y] {
                    self.pc += 2;
                }
            },
            0x6000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let nn = (self.opcode & 0x00FF) as u8;
                self.V[x] = nn;
            },
            0x7000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let nn = (self.opcode & 0x00FF) as u8;

                self.V[x] = (Wrapping(self.V[x]) + Wrapping(nn)).0;
            },
            0x8000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let y = ((self.opcode & 0x00F0) >> 4) as usize;
                match self.opcode & 0x000F {
                    0 => {
                        self.V[x] = self.V[y];
                    },
                    1 => {
                        self.V[x] = self.V[x] | self.V[y];
                    },
                    2 => {
                        self.V[x] = self.V[x] & self.V[y];
                    },
                    3 => {
                        self.V[x] = self.V[x] ^ self.V[y];
                    },
                    4 => {
                        self.V[x] = (Wrapping(self.V[x]) + Wrapping(self.V[y])).0;

                        if self.V[x] < self.V[y] {
                            self.V[0xF] = 1
                        } else {
                            self.V[0xF] = 0
                        }
                    },
                    5 => {
                        self.V[x] = (Wrapping(self.V[x]) - Wrapping(self.V[y])).0;

                        if (Wrapping(self.V[x]) + Wrapping(self.V[y])).0 > self.V[y] {
                            self.V[0xF] = 1
                        } else {
                            self.V[0xF] = 0
                        }
                    },
                    6 => {
                        if (self.V[x] & 1) == 1 {
                            self.V[0xF] = 1
                        } else {
                            self.V[0xF] = 0
                        }

                        self.V[x] = self.V[x] >> 1;
                    },
                    7 => {
                        if self.V[y] > self.V[x] {
                            self.V[0xF] = 1
                        } else {
                            self.V[0xF] = 0
                        }

                        self.V[x] = self.V[y] - self.V[x]
                    },
                    0xE => {
                        if (self.V[x] >> 7) == 1  {
                            self.V[0xF] = 1
                        } else {
                            self.V[0xF] = 0
                        }

                        self.V[x] = self.V[x] << 1;
                    },
                    _ => {}
                }
            },
            0x9000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let y = ((self.opcode & 0x00F0) >> 4) as usize;

                if self.V[x] != self.V[y] {
                    self.pc += 2;
                }
            },
            0xA000 => {
                let nnn = (self.opcode & 0x0FFF) as usize;
                self.I = nnn;
            },
            0xB000 => {
                let nnn = (self.opcode & 0x0FFF) as usize;

                self.pc = nnn + self.V[0] as usize
            },
            0xC000 => {
                let x = ((self.opcode & 0x0F00) >> 8) as usize;
                let nn = (self.opcode & 0x00FF) as u8;

                self.V[x] = rand::random::<u8>() & nn
            },
            0xD000 => {
                let x = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                let y = self.V[((self.opcode & 0x00F0) >> 4) as usize];
                let height = (self.opcode & 0x000F) as u8;
                let mut pixel: u8;

                self.V[0xF as usize] = 0;
                for j in 0..height {
                    pixel = self.mem[self.I + j as usize];
                    for i in 0..8 {
                        if (pixel & (0x80 >> i)) > 0 {
                            if self.gfx[((x + i) as u16 + (y + j) as u16 * 64) as usize] == 1 {
                                self.V[0xF as usize] = 1;
                            }
                            self.gfx[((x + i) as u16 + (y + j) as u16 * 64) as usize] ^= 1;
                        }
                    }
                }
            },
            0xE000 => {
                match self.opcode & 0x00FF {
                    // fix
                    // keyboard handle
                    0x009E => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        let x_key;
                        match self.V[x] {
                            0 => x_key = Key::Key0,
                            1 => x_key = Key::Key1,
                            2 => x_key = Key::Key2,
                            3 => x_key = Key::Key3,
                            4 => x_key = Key::Key4,
                            5 => x_key = Key::Key5,
                            6 => x_key = Key::Key6,
                            7 => x_key = Key::Key7,
                            8 => x_key = Key::Key8,
                            9 => x_key = Key::Key9,
                            0xA => x_key = Key::A,
                            0xB => x_key = Key::B,
                            0xC => x_key = Key::C,
                            0xD => x_key = Key::D,
                            0xE => x_key = Key::E,
                            0xF => x_key = Key::F,
                            _ => x_key = Key::Unknown
                        }

                        if self.window.is_key_down(x_key) {
                            self.pc += 2
                        }
                    },
                    0x00A1 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        let x_key;
                        match self.V[x] {
                            0 => x_key = Key::Key0,
                            1 => x_key = Key::Key1,
                            2 => x_key = Key::Key2,
                            3 => x_key = Key::Key3,
                            4 => x_key = Key::Key4,
                            5 => x_key = Key::Key5,
                            6 => x_key = Key::Key6,
                            7 => x_key = Key::Key7,
                            8 => x_key = Key::Key8,
                            9 => x_key = Key::Key9,
                            0xA => x_key = Key::A,
                            0xB => x_key = Key::B,
                            0xC => x_key = Key::C,
                            0xD => x_key = Key::D,
                            0xE => x_key = Key::E,
                            0xF => x_key = Key::F,
                            _ => x_key = Key::Unknown
                        }

                        if !self.window.is_key_down(x_key) {
                            self.pc += 2
                        }
                    },
                    _ => {}
                }
            },
            0xF000 => {
                match self.opcode & 0x00FF {
                    0x0007 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        self.V[x] = self.dt
                    },
                    0x000A => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        loop {
                            let keys_pressed = self.window.get_keys();
                            if keys_pressed.len() > 0 {
                                match keys_pressed[0] {
                                    Key::Key0 => self.V[x] = 0,
                                    Key::Key1 => self.V[x] = 1,
                                    Key::Key2 => self.V[x] = 2,
                                    Key::Key3 => self.V[x] = 3,
                                    Key::Key4 => self.V[x] = 4,
                                    Key::Key5 => self.V[x] = 5,
                                    Key::Key6 => self.V[x] = 6,
                                    Key::Key7 => self.V[x] = 7,
                                    Key::Key8 => self.V[x] = 8,
                                    Key::Key9 => self.V[x] = 9,
                                    Key::A => self.V[x] = 0xA,
                                    Key::B => self.V[x] = 0xB,
                                    Key::C => self.V[x] = 0xC,
                                    Key::D => self.V[x] = 0xD,
                                    Key::E => self.V[x] = 0xE,
                                    Key::F => self.V[x] = 0xF,
                                    _ => continue
                                }
                                break
                            }
                        }
                    },
                    0x0015 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        self.dt = self.V[x]
                    },
                    0x0018 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        self.st = self.V[x]
                    },
                    0x001E => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        self.I += self.V[x] as usize
                    },
                    0x0029 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        self.I = (self.V[x] * 5) as usize
                    },
                    0x0033 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        self.mem[self.I] = self.V[x] / 100;
                        self.mem[self.I + 1] = (self.V[x] % 100) / 10;
                        self.mem[self.I + 2] = self.V[x] % 10;
                    },
                    0x0055 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        for i in 0..(x + 1) {
                            self.mem[self.I + i] = self.V[i]
                        }
                    },
                    0x0065 => {
                        let x = ((self.opcode & 0x0F00) >> 8) as usize;

                        for i in 0..(x + 1) {
                            self.V[i] = self.mem[self.I + i]
                        }
                    },
                    _ => {}
                }
            },
            _ => {}
        }

        self.pc += 2;

    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let chip8 = Chip8::new();

        //chip8.mem[512] = 0xA0;
        //chip8.mem[513] = 0x4B;

        //chip8.mem[514] = 0x60;
        //chip8.mem[515] = 0x00;

        //chip8.mem[516] = 0x61;
        //chip8.mem[517] = 0x00;

        //chip8.mem[518] = 0xD0;
        //chip8.mem[519] = 0x15;

        //chip8.mem[520] = 0x12;
        //chip8.mem[521] = 0x08;

        //let initial_pc = 0x200;

        //assert_eq!(chip8.pc, initial_pc);

        //chip8.run_cycle();
        //assert_eq!(chip8.I, 0x4B);
        //assert_eq!(chip8.pc, initial_pc + 2);

        //chip8.run_cycle();
        //assert_eq!(chip8.V[0], 0);
        //assert_eq!(chip8.pc, initial_pc + 4);

        //chip8.run_cycle();
        //assert_eq!(chip8.V[1], 0);
        //assert_eq!(chip8.pc, initial_pc + 6);

        //chip8.run_cycle();
        //assert_eq!(chip8.gfx[0], 1);
        //assert_eq!(chip8.gfx[1], 1);
        //assert_eq!(chip8.gfx[2], 1);
        //assert_eq!(chip8.gfx[3], 1);
        //assert_eq!(chip8.gfx[4], 0);
    }
}
